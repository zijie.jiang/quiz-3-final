package com.twuc.product;

import com.twuc.JpaBaseTest;
import com.twuc.contract.product.ContractProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProductTest extends JpaBaseTest {
    @Test
    void should_return_200_when_get_products() throws Exception {
        mockGet("/api/products").andExpect(status().isOk());
    }

    @Test
    void should_return_201_when_add_product() throws Exception {
        ContractProductRequest request = new ContractProductRequest(
                "旺旺牛奶",
                new BigDecimal(10),
                "瓶",
                "https://img.365diandao.com/Storage/Shop/317/Products/3586/1.png"
        );
        mockPost("/api/products", request, MediaType.APPLICATION_JSON)
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/products"));

        mockGet("/api/products")
                .andExpect(jsonPath("$[0].name").value("旺旺牛奶"));
    }
}
