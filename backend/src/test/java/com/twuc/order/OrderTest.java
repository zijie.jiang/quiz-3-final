package com.twuc.order;

import com.twuc.JpaBaseTest;
import com.twuc.contract.product.ContractProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderTest extends JpaBaseTest {
    @Test
    void should_return_200_when_get_order_list() throws Exception {
        mockGet("/api/orders").andExpect(status().isOk());
    }

    @Test
    void should_return_201_when_add_order_given_product_id() throws Exception {
        ContractProductRequest request = new ContractProductRequest(
                "旺旺牛奶",
                new BigDecimal(10),
                "瓶",
                "https://img.365diandao.com/Storage/Shop/317/Products/3586/1.png"
        );
        mockPost("/api/products", request, MediaType.APPLICATION_JSON)
                .andExpect(status().isCreated());

        mockPost("/api/orders?productId=1")
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/orders"));
    }

    @Test
    void should_return_200_when_delete_order_given_order_id() throws Exception {
        ContractProductRequest request = new ContractProductRequest(
                "旺旺牛奶",
                new BigDecimal(10),
                "瓶",
                "https://img.365diandao.com/Storage/Shop/317/Products/3586/1.png"
        );
        mockPost("/api/products", request, MediaType.APPLICATION_JSON)
                .andExpect(status().isCreated());

        mockPost("/api/orders?productId=1")
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/orders"));

        mockDelete("/api/orders/1").andExpect(status().isOk());
    }
}
