package com.twuc.domain.product.entity;

import com.twuc.contract.product.ContractProductRequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 64)
    private String name;

    @Column(nullable = false, precision = 8, scale = 2)
    private BigDecimal price;

    @Column(nullable = false, length = 8)
    private String unit;

    @Column(length = 64)
    private String imageUrl;

    public Product() {
    }

    public Product(ContractProductRequest contractProductRequest) {
        this.name = contractProductRequest.getName();
        this.price = contractProductRequest.getPrice();
        this.unit = contractProductRequest.getUnit();
        this.imageUrl = contractProductRequest.getImageUrl();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
