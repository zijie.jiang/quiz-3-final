package com.twuc.web;

import com.twuc.contract.order.ContractOrderResponse;
import com.twuc.domain.order.OrderRepository;
import com.twuc.domain.order.entity.Order;
import com.twuc.domain.product.ProductRepository;
import com.twuc.domain.product.entity.Product;
import com.twuc.exception.OrderException;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin
public class OrderController {

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    public OrderController(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<List> findAll() {
        List<ContractOrderResponse> responses = new ArrayList<>();
        List<Order> orders = orderRepository.findAll();
        orders.forEach(order -> responses.add(new ContractOrderResponse(order)));
        return ResponseEntity.ok(responses);
    }

    @PostMapping
    public ResponseEntity<Void> addOrder(@RequestParam Long productId) {
        URI uri = linkTo(methodOn(OrderController.class).findAll()).toUri();
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (!optionalProduct.isPresent()) {
            throw new OrderException("product does not exist");
        }
        List<Order> orders = orderRepository.findByProductId(productId);
        if (orders.isEmpty()) {
            Order order = new Order(optionalProduct.get());
            orderRepository.save(order);
        } else if (orders.size() == 1) {
            Order order = orders.get(0);
            order.setQuantity(order.getQuantity() + 1);
            orderRepository.save(order);
        } else {
            throw new OrderException("an order mapping multiple product");
        }
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<Void> deleteOrder(@PathVariable Long orderId) {
        orderRepository.deleteById(orderId);
        return ResponseEntity.ok().build();
    }
}
