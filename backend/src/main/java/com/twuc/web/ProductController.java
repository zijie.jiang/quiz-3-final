package com.twuc.web;

import com.twuc.contract.product.ContractProductRequest;
import com.twuc.contract.product.ContractProductResponse;
import com.twuc.domain.product.ProductRepository;
import com.twuc.domain.product.entity.Product;
import com.twuc.exception.ProductException;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(exposedHeaders = "LOCATION")
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<List> findAll() {
        List<ContractProductResponse> responses = new ArrayList<>();
        List<Product> products = productRepository.findAll();
        products.forEach(product -> responses.add(new ContractProductResponse(product)));
        return ResponseEntity.ok(responses);
    }

    @PostMapping
    public ResponseEntity<Long> addProduct(@RequestBody ContractProductRequest contractProductRequest) {
        URI uri = linkTo(methodOn(ProductController.class).findAll()).toUri();
        Optional<Product> optionalProduct = productRepository.findByName(contractProductRequest.getName());
        if (optionalProduct.isPresent()) {
            throw new ProductException("product name already exist");
        }
        Product savedProduct = productRepository.save(contractProductRequest.toProduct());

        return ResponseEntity.created(uri).body(savedProduct.getId());
    }
}
