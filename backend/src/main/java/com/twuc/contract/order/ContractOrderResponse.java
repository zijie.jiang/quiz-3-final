package com.twuc.contract.order;

import com.twuc.domain.order.entity.Order;

import java.math.BigDecimal;

public class ContractOrderResponse {

    private Long id;

    private Integer quantity;

    private String productName;

    private BigDecimal productPrice;

    private String productUnit;

    public ContractOrderResponse() {
    }

    public ContractOrderResponse(Order order) {
        this.id = order.getId();
        this.quantity = order.getQuantity();
        this.productName = order.getProduct().getName();
        this.productPrice = order.getProduct().getPrice();
        this.productUnit = order.getProduct().getUnit();
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getProductName() {
        return productName;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public String getProductUnit() {
        return productUnit;
    }
}
