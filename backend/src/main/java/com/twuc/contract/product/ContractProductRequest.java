package com.twuc.contract.product;

import com.twuc.domain.product.entity.Product;

import java.math.BigDecimal;

public class ContractProductRequest {

    private String name;

    private BigDecimal price;

    private String unit;

    private String imageUrl;

    public ContractProductRequest() {
    }

    public ContractProductRequest(String name,
                                  BigDecimal price,
                                  String unit,
                                  String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Product toProduct() {
        return new Product(this);
    }
}
