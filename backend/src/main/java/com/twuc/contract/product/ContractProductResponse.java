package com.twuc.contract.product;

import com.twuc.domain.product.entity.Product;

import java.math.BigDecimal;

public class ContractProductResponse {

    private Long id;

    private String name;

    private BigDecimal price;

    private String unit;

    private String imageUrl;

    public ContractProductResponse() {
    }

    public ContractProductResponse(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.unit = product.getUnit();
        this.imageUrl = product.getImageUrl();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
