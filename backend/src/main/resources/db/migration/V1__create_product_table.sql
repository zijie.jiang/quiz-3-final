CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(64) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `unit` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
