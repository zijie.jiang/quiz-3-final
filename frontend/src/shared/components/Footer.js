import React from "react";
import '../../../styles/Footer.less'

function Footer() {
    return (
        <span className='mall-footer'>
            TW Mall @2018 Created by ForCheng
        </span>
    )
}

export default Footer;
