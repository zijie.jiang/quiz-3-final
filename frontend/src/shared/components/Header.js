import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {MdAdd, MdHome, MdShoppingCart} from "react-icons/md";
import '../../../styles/Header.less'

export const PRODUCT_LIST = 'product list';
export const ORDER_LIST = 'order list';
export const CREATE_PRODUCT = 'create product';

class Header extends Component {

    constructor(props, context) {
        super(props, context);
        this.selected = this.selected.bind(this);
    }

    render() {
        return (
            <nav className='mall-header'>
                <Link to='/' style={this.selected(PRODUCT_LIST)}><MdHome/><span>商城</span></Link>
                <Link to='/orders' style={this.selected(ORDER_LIST)}><MdShoppingCart/><span>订单</span></Link>
                <Link to='/product/create' style={this.selected(CREATE_PRODUCT)}><MdAdd/><span>添加商品</span></Link>
            </nav>
        );
    }

    selected(keyword) {
        const {location} = this.props;
        if (location === keyword) {
            return {backgroundColor: '#1690FF'}
        }
    }
}

export default Header;
