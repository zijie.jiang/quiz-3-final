import React, {Component} from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import Product from "./product/pages/Product";
import Order from "./order/pages/Order";
import ProductProducer from "./product/pages/ProductProducer";
import '../styles/App.less'

class App extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Route exact path='/' component={Product}/>
                    <Route exact path='/orders' component={Order} />
                    <Route exact path='/product/create' component={ProductProducer} />
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
