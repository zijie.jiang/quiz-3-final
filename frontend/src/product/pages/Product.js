import React, {Component} from 'react';
import {connect} from "react-redux";
import Header, {PRODUCT_LIST} from "../../shared/components/Header";
import ProductItem from "../components/ProductItem";
import {addOrder, getProducts} from "../actions/ProductActions";
import '../../../styles/Product.less';
import Footer from "../../shared/components/Footer";

class Product extends Component {
    render() {
        const {products, addOrder, state} = this.props;
        return (
            <div>
                <Header location={PRODUCT_LIST} />
                <section className='product-list'>
                    <ul>
                        {
                            products.map((item, index) => <ProductItem key={index} product={item} addOrder={addOrder} state={state}/>)
                        }
                    </ul>
                    <Footer/>
                </section>
            </div>
        );
    }

    componentDidMount() {
        const {getProducts} = this.props;
        getProducts();
    }
}

const mapStateToProps = ({productReducers}) => ({
    products: productReducers.products,
    state: productReducers.state
});

const mapDispatchToProps = (dispatch) => ({
    getProducts: () => dispatch(getProducts()),
    addOrder: id => dispatch(addOrder(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
