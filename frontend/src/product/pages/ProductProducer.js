import React, {Component} from 'react';
import Header, {CREATE_PRODUCT, PRODUCT_LIST} from "../../shared/components/Header";
import {addProduct} from "../actions/ProductActions";
import {connect} from "react-redux";
import '../../../styles/ProductProducer.less';
import Footer from "../../shared/components/Footer";

class ProductProducer extends Component {

    constructor(props, context) {
        super(props, context);
        this.bindProduct = this.bindProduct.bind(this);
        this.addProduct = this.addProduct.bind(this);
        this.check = this.check.bind(this);
        this.state = {
            product: {
                name: '',
                price: 0,
                unit: '',
                imageUrl: ''
            }
        }
    }

    render() {
        const disable = this.check();
        return (
            <div>
                <Header location={CREATE_PRODUCT} />
                <section className='product'>
                    <form>
                        <h1>添加商品</h1>
                        <label>
                            <i>*</i>名称:
                            <input type='text' placeholder='名称' onChange={e => this.bindProduct(e, 'name')}/>
                        </label>
                        <label>
                            <i>*</i>价格:
                            <input type='text' placeholder='价格' onChange={e => this.bindProduct(e, 'price')}/>
                        </label>
                        <label>
                            <i>*</i>单位:
                            <input type='text' placeholder='单位' onChange={e => this.bindProduct(e, 'unit')}/>
                        </label>
                        <label>
                            <i>*</i>图片:
                            <input type='text' placeholder='URL' onChange={e => this.bindProduct(e, 'imageUrl')}/>
                        </label>
                        <button type='submit' disabled={disable} onClick={this.addProduct}>提交</button>
                    </form>
                    <Footer/>
                </section>
            </div>
        );
    }

    addProduct(e) {
        e.preventDefault();
        const {addProduct, history} = this.props;
        const {product} = this.state;
        addProduct(product, history)
    }

    bindProduct(e, keyword) {
        e.preventDefault();
        const {product} = this.state;
        switch (keyword) {
            case 'name':
                this.setState({
                    product: {
                        ...product,
                        name: e.target.value
                    }
                });
                break;
            case 'price':
                if (isNaN(e.target.value)) {
                    return;
                }
                this.setState({
                    product: {
                        ...product,
                        price: parseInt(e.target.value)
                    }
                });
                break;
            case 'unit':
                this.setState({
                    product: {
                        ...product,
                        unit: e.target.value
                    }
                });
                break;
            case 'imageUrl':
                this.setState({
                    product: {
                        ...product,
                        imageUrl: e.target.value
                    }
                });
                break;
        }
    }

    check() {
        const {name, price, unit, imageUrl} = this.state.product;
        return !(name !== '' && price > 0 && unit !== '' && imageUrl !== '');
    }
}

const mapDispatchToProps = (dispatch) => ({
    addProduct: (product, history) => dispatch(addProduct(product, history))
});

export default connect(undefined, mapDispatchToProps)(ProductProducer);
