import {addOrderApi, addProductApi, getProductsApi} from "../api/productApi";
import refresh from "../../shared/components/refreshOrders";
export const GET_PRODUCTS = 'get products';
export const ADD_ORDER = 'add order';
export const ADD_PRODUCT = 'add product';

function getProducts() {
    return (dispatch) => getProductsApi().then(json => {
        dispatch({
            type: GET_PRODUCTS,
            payload: json
        })
    })
}

function addOrder(productId) {
    return (dispatch) => addOrderApi(productId).then(state => {
        dispatch({
            type: ADD_ORDER,
            payload: state
        })
    })
}

function addProduct(product, history) {
    return (dispatch) => addProductApi(product).then(state => {
        dispatch({
            type: ADD_PRODUCT
        });
        if (!state) {
            alert("商品名称已存在，请输入新的商品名称");
        }
        refresh(history);
    })
}

export {
    getProducts,
    addOrder,
    addProduct
}

