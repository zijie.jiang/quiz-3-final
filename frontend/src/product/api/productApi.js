const URL = 'http://localhost:8080/api';


function getProductsApi() {
    return fetch(URL + "/products").then(response => response.json());
}

function addOrderApi(productId) {
    return fetch(URL + "/orders?productId=" + productId, {
        method: 'POST'
    }).then(response => response.ok).catch(() => false)
}

function addProductApi(product) {
    console.log(product);
    return fetch(URL + "/products", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
    }).then(response => {
        return response.ok
    })
}

export {
    getProductsApi,
    addOrderApi,
    addProductApi
}
