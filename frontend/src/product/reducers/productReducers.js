import {ADD_ORDER, ADD_PRODUCT, GET_PRODUCTS} from "../actions/ProductActions";

const initState = {
    products: [],
    state: true
};

export default function productReducers(state = initState, action) {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            };
        case ADD_ORDER:
            return {
                ...state,
                state: action.payload
            };
        case ADD_PRODUCT:
            return state;
        default:
            return state;
    }
}
