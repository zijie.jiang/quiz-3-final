import React, {Component} from 'react';
import '../../../styles/ProductItem.less';

class ProductItem extends Component {

    constructor(props, context) {
        super(props, context);
        this.addProduct = this.addProduct.bind(this);
    }

    render() {
        const {name, price, unit, imageUrl} = this.props.product;
        const {state} = this.props;
        return (
            <li>
                <img src={imageUrl} alt='product image'/>
                <div>{name}</div>
                <span>单价:{price}元/{unit}</span>
                <button disabled={!state} onClick={this.addProduct}>
                    +
                </button>
            </li>
        );
    }

    addProduct() {
        const {id} = this.props.product;
        const {addOrder} = this.props;
        addOrder(id);
    }
}

export default ProductItem;
