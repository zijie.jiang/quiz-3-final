import {combineReducers} from "redux";
import productReducers from "./product/reducers/productReducers";
import orderReducers from "./order/reducers/orderReducers";

const reducers = combineReducers({
    productReducers,
    orderReducers
});

export default reducers;
