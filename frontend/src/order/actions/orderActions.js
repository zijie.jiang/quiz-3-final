import {deleteOrderApi, getOrdersApi} from "../api/ordersApi";

export const GET_ORDERS = 'get orders';
export const DELETE_ORDER = 'delete orders';

function getOrders() {
    return (dispatch) => getOrdersApi().then(json => {
        dispatch({
            type: GET_ORDERS,
            payload: json
        })
    })
}

function deleteOrder(orderId) {
    return (dispatch) => deleteOrderApi(orderId).then(state => {
        dispatch({
            type: DELETE_ORDER,
            payload: state
        });
    })
}

export {
    getOrders,
    deleteOrder
}
