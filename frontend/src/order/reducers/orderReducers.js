import {DELETE_ORDER, GET_ORDERS} from "../actions/orderActions";

const initState = {
    orders: [],
    state: undefined
};

export default function orderReducers(state = initState, action) {
    switch (action.type) {
        case GET_ORDERS:
            return {
                ...state,
                orders: action.payload,
                state: undefined
            };
        case DELETE_ORDER:
            return {
                ...state,
                orders: action.payload ? [] : state.orders,
                state: action.payload
            };
        default:
            return state;
    }

}
