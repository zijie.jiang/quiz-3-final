const URL = 'http://localhost:8080/api';


function getOrdersApi() {
    return fetch(URL + "/orders").then(response => response.json());
}

function deleteOrderApi(orderId) {
    return fetch(URL + "/orders/" + orderId, {
        method: 'DELETE'
    }).then(response => response.ok).catch(() => false);
}

export {
    getOrdersApi,
    deleteOrderApi
}
