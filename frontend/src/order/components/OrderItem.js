import React, {Component} from 'react';

class OrderItem extends Component {

    constructor(props, context) {
        super(props, context);
        this.deleteOrder = this.deleteOrder.bind(this);
    }

    render() {
        const {order} = this.props;
        return (
            <tr>
                <td>{order.productName}</td>
                <td>{order.productPrice}</td>
                <td>{order.quantity}</td>
                <td>{order.productUnit}</td>
                <td><button onClick={this.deleteOrder}>删除</button></td>
            </tr>
        );
    }

    deleteOrder() {
        const {deleteOrder} = this.props;
        const {id} = this.props.order;
        deleteOrder(id);
    }
}

export default OrderItem;
