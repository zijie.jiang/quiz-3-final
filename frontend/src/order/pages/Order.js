import React, {Component} from 'react';
import Header, {ORDER_LIST} from "../../shared/components/Header";
import {connect} from "react-redux";
import {deleteOrder, getOrders} from "../actions/orderActions";
import OrderItem from "../components/OrderItem";
import {Link} from "react-router-dom";
import '../../../styles/Order.less';
import Footer from "../../shared/components/Footer";

class Order extends Component {

    constructor(props, context) {
        super(props, context);
        this.checkState = this.checkState.bind(this);
    }

    render() {
        this.checkState();
        const {orders, deleteOrder} = this.props;
        if (orders.length === 0) {
            return (
                <h3>
                    暂无订单，返回<Link to='/'>商城页面</Link>继续购买
                </h3>
            )
        }
        return (
            <div>
                <Header location={ORDER_LIST}/>
                <section className='order-list'>
                    <table>
                        <thead>
                        <tr>
                            <th>名字</th>
                            <th>单价</th>
                            <th>数量</th>
                            <th>单位</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            orders.map((item, index) => <OrderItem key={index} deleteOrder={deleteOrder} order={item}/>)
                        }
                        </tbody>
                    </table>
                    <Footer/>
                </section>
            </div>
        );
    }

    checkState() {
        const {state} = this.props;
        if (state) {
            this.componentDidMount();
        }
        if (state === false) {
            alert("商品删除失败，请稍后再试");
        }
    }

    componentDidMount() {
        const {getOrders} = this.props;
        getOrders();
    }
}

const mapStateToProps = ({orderReducers}) => ({
    orders: orderReducers.orders,
    state: orderReducers.state
});

const mapDispatchToProps = (dispatch) => ({
    getOrders: () => dispatch(getOrders()),
    deleteOrder: (orderId) => dispatch(deleteOrder(orderId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Order);
